import UIKit
import CoreLocation

class ViewController: UIViewController {
    @IBOutlet weak var nameCityLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private let viewModel: ViewModel = ViewModel()
    private let locationModel = LocationModel.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationModel.locationManager.delegate = self
        locationModel.initLocation()
        tableView.register(UINib (
                            nibName: "TableViewCell",
                            bundle: nil),
                           forCellReuseIdentifier: "TableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isHidden = true
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        self.viewModel.getDataWeather(locValue: userLocation.coordinate, completion: { isUpdate in
            if isUpdate {
                DispatchQueue.main.async {
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                    self.nameCityLbl.text = self.viewModel.weaterData?.city ?? ""
                }
            }
        })
    }
        
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
}

extension ViewController: UITableViewDelegate {    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.weaterData?.weather.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let items = viewModel.weaterData?.weather {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell
            cell.configure(item: items[indexPath.row])
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            return cell
        }
        return UITableViewCell()
    }
}
