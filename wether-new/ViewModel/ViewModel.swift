import Foundation
import CoreLocation

class ViewModel {
    private let model = WeatherModel()
    var weaterData: DataItem?
    
    init() {
        
    }
    
    func getDataWeather(locValue: CLLocationCoordinate2D, completion: @escaping (Bool) -> Void) {
        model.fetchSummary(locValue: locValue, completion: { wheatherItems in
            completion(true)
            self.weaterData = wheatherItems
        })
    }
}
